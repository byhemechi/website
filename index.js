const express = require('express');
const app = express()
const helmet = require('helmet');
const cors = require('cors');
const port = Number(process.env.PORT || 443);

app.use(cors())

// app.use(require('cors')())

app.use(helmet({
  hsts: {
    maxAge: 31536000,
    includeSubDomains: true,
    preload: true
  },
  referrerPolicy: {
     policy: 'same-origin'
  },
  contentSecurityPolicy: {
    directives: {
      defaultSrc: ["'self'", 'cdnjs.cloudflare.com'],
      styleSrc: ["'self'", "'unsafe-inline'", 'https://fonts.googleapis.com', "https://cdnjs.cloudflare.com"],
      fontSrc: ["'self'", 'https://fonts.gstatic.com']
    }
  },
  expectCT: {
    enforce: true,
    maxAge: 30,
    reportUri: "https://hemechi.report-uri.com/r/d/ct/enforce"
  }
}))

app.use(function(req, res, next) {
  res.set('Feature-Policy', "accelerometer 'none'; camera 'none'; geolocation 'none'; gyroscope 'none'; magnetometer 'none'; microphone 'none'; payment 'none'; usb 'none'");
  next()
})

app.use("/chi/", express.static(__dirname + "/chi"))

app.all(/(^(?!\/chi).*)/, function(req, res) {
  // console.log(req.url)
  res.redirect(301, "/chi" + req.url)
})


app.listen()
