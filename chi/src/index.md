---
title: byhemechi
description: the website of the man himself, splorge fisker
slug: home
bg: pano.jpg
showtitle: false
---

# G'Day, I'm George
I'm a freelance software guy: I start a lot of projects and sometimes I finish them.

I mainly write javascript (please don't kill me) but I'm also pretty good with Dart and Python, and have made a fair few things with them over time.

I started with programming when I was 12 with PHP, soon realised that other languages were infinitely better and promptly forgot most of what I knew about PHP. Over the years I've amassed a lot of unfinished things on [CSSDeck] and CodePen, which looking back seem to be mostly shitty markdown editors.
# Current projects
+ [cmx2](/chi/cmx2)
# Finished/handed over
+ [Echidna Rocktools](https://echidna-rocktools.eu)  
  The website for an earthmoving company
+ [SbubHub](https://pornhub.byhe.me/chi)  
  I was really bored in class
+ [Speed Saved me](https://pagge-fzjwogfkhn.now.sh)  
  A class project: preventing road fatigue using methamphetamines