
module.exports = {
    "build": [{
        "match": [/\.html?$/i]
    }, {
        "match": [/\.md$/i, /\.markdown$/i],
        "transformers": [
            {"name": "markdown-it"},
            {"name": "twemoji"}
        ]
    }]
}